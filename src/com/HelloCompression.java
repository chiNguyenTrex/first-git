package com;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.security.KeyStore.Entry;
import java.text.Normalizer;
import java.text.Normalizer.Form;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.SortedMap;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

import javax.xml.bind.annotation.adapters.NormalizedStringAdapter;

public class HelloCompression {
	public static void main(String[] args) {
		//System.setProperty("zip.altEncoding", "Utf-8");
		ZipInputStream zis = null;
		FileInputStream fis = null;
		ZipEntry entry = null;

		try {
			System.out.println("This file included: ");	
			fis = new FileInputStream("com.zip");
			zis = new ZipInputStream(fis, Charset.forName("Cp1258"));
			while ( (entry = zis.getNextEntry()) != null ) {
				if (entry.isDirectory()) {
					System.out.print("  -Directory: ");
				} else {
					System.out.print("  +File: ");
				}
				System.out.println(Normalizer.normalize(entry.getName(), Form.NFC));
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			zis.closeEntry();
			zis.close();
			fis.close();

		} catch (IOException e) {
			e.printStackTrace();
		}

		 System.out.println(Charset.isSupported("cp1258")); // Kiểm tra liệu có
		 
		// hổ trợ bảng mã này ko

		/*
		 * in ra tất cả các mã trong hệ thống SortedMap<String, Charset> map =
		 * Charset.availableCharsets(); Set<String> set = map.keySet();
		 * 
		 * Iterator<String> ite = set.iterator(); String key; while (
		 * ite.hasNext()) { key = ite.next(); System.out.println("Key: " + key +
		 * "\t\tValue: " + map.get(key)); }
		 * 
		 */

	}

}
