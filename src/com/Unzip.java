package com;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

public class Unzip {
	public static void main(String[] args) {
		ZipInputStream zis = null;

		byte[] buffer = new byte[1024];
		ZipEntry entry = null;

		try {
			zis = new ZipInputStream(new FileInputStream("com.zip"), Charset.forName("CP437"));
			while ((entry = zis.getNextEntry()) != null) {
				String nameEntry = entry.getName();
				System.out.println("Unzip :" + nameEntry);

				if (entry.isDirectory()) {
					new File(nameEntry).mkdirs();
				} else {
					FileOutputStream fos = new FileOutputStream(nameEntry);
					int len;
					while ((len = zis.read(buffer)) > 0) {
						fos.write(buffer, 0, len);
					}
					
					fos.close();

				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				zis.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
}
